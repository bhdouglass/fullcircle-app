import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Lomiri.Components 1.3 as Lomiri
import QtQuick.Controls.Suru 2.2

MouseArea {
    property var issue
    property double preferredHeight: ubuntuShapeIcon.height + label.height + units.gu(1)

    ColumnLayout {
        anchors.fill: parent
        spacing: units.gu(1)

        Rectangle {
            opacity: issue.downloaded ? 1 : 0.3
            color: Suru.backgroundColor

            Layout.fillWidth: true
            Layout.leftMargin: units.gu(4)
            Layout.rightMargin: units.gu(4)
            Layout.preferredHeight: width * (7/8)

            Lomiri.LomiriShape {
                id: ubuntuShapeIcon

                anchors.fill: parent

                image: Image {
                    id: cover
                    Layout.fillWidth: true
                    Layout.leftMargin: units.gu(4)
                    Layout.rightMargin: units.gu(4)
                    Layout.preferredHeight: width * (7/8)

                    source: Qt.resolvedUrl(issue.image)
                    fillMode: Image.PreserveAspectFit
                    sourceSize {
                        width: ubuntuShapeIcon.width
                        height: ubuntuShapeIcon.height
                    }
                }
            }

            Image {
                anchors.fill: ubuntuShapeIcon
                visible: cover.progress < 1

                source: Qt.resolvedUrl('../../assets/emptyImage.svg')
                fillMode: Image.PreserveAspectFit
                sourceSize {
                    width: ubuntuShapeIcon.width
                    height: ubuntuShapeIcon.height
                }
            }
        }

        Label {
            id: label
            Layout.fillWidth: true

            text: issue.title
            horizontalAlignment: Label.AlignHCenter

            color: issue.downloaded ? Lomiri.LomiriColors.orange : Suru.neutralColor
        }
    }
}
