import requests
import xml.etree.ElementTree as ET
import html
import re

RSS_FEED = 'https://fullcirclemagazine.org/magazines/index.xml'


def fetchIssues():
    response = requests.get(RSS_FEED)
    response.raise_for_status()  # TODO

    root = ET.fromstring(response.text)
    issues = []
    for item in root.iter('item'):
        url = item.findtext('link')
        issueNumber = int(url.replace(
            'https://fullcirclemagazine.org/magazines/issue-',
            ''
        ).replace('/', ''))

        image = ''
        downloads = []
        for enclosure in item.iter('enclosure'):
            if enclosure.attrib['type'] and enclosure.attrib['url']:
                if enclosure.attrib['type'] == 'image/webp':
                    image = enclosure.attrib['url']
                else:
                    downloads.append({
                        'lang': 'English',
                        'url': enclosure.attrib['url'],
                        'type': 'pdf' if enclosure.attrib['url'].endswith('.pdf') else 'epub',
                    })

        # Wrap the description in a random element so there is only one root for etree to parse
        description = item.findtext('description')
        description = html.unescape(description).replace('&', '&amp;').replace('<hr>', '')
        description = re.sub(r'<img[^>]+>', '', description)
        description = '<html>' + \
            description + \
            '</html>'
        descriptionRoot = ET.fromstring(description)
        uls = descriptionRoot.findall('ul')

        descriptionText = []
        if len(uls) > 1:
            for li in uls[0].iter('li'):
                text = li.text
                if text and text.strip():
                    descriptionText.append(text)

                text = li.findtext('*')
                if text and text.strip():
                    descriptionText.append(text)

            for li in uls[-1].iter('li'):
                a = li.find('a')
                if a is not None and a.text and a.attrib['href']:
                    downloads.append({
                        'lang': a.text.replace('PDF', '').replace('EPUB', '').strip(),
                        'url': a.attrib['href'],
                        'type': 'pdf' if a.attrib['href'].endswith('.pdf') else 'epub',
                    })

        issues.append({
            'title': item.findtext('title').replace('Full Circle Magazine', 'Issue'),
            'url': url,
            'description': descriptionText,
            'image': image,
            'id': '{:03d}'.format(issueNumber),
            'downloads': downloads,
        })

    return issues


if __name__ == "__main__":
    import json

    issues = fetchIssues()
    print(json.dumps(issues, indent=2))
