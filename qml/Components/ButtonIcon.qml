import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Lomiri.Components 1.3 as Lomiri

ToolButton {
    property alias iconName: icon.name

    Layout.fillHeight: true
    Layout.preferredWidth: height
    Layout.topMargin: units.gu(1.5)
    Layout.bottomMargin: units.gu(1.5)
    Layout.rightMargin: units.gu(.5)

    contentItem: Lomiri.Icon {
        id: icon
        color: colors.text
    }

    // Don't show the pressed background
    background: Rectangle { visible: false }
}
