import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Lomiri.Components 1.3 as Lomiri

ToolBar {
    property alias title: titleLabel.text
    property alias showBack: backButton.visible
    property alias showAbout: aboutButton.visible

    signal backButtonClicked()
    signal aboutButtonClicked()

    height: units.gu(6)

    property QtObject colors: QtObject {
        readonly property color divider: Lomiri.LomiriColors.orange
        readonly property color text: '#f5f5f5'
        readonly property color background: '#000000'
    }

    RowLayout {
        spacing: units.gu(.25)

        anchors {
            fill: parent
            leftMargin: backButton.visible ? units.gu(.5) : units.gu(1)
        }

        ButtonIcon {
            id: backButton
            iconName: 'previous'

            onClicked: backButtonClicked()
        }

        Label {
            id: titleLabel
            Layout.fillWidth: true
            color: colors.text

            font.pixelSize: FontUtils.sizeToPixels('large')
            verticalAlignment: Qt.AlignVCenter
            wrapMode: Text.NoWrap
            elide: Text.ElideRight
        }

        ButtonIcon {
            id: aboutButton
            iconName: 'info'

            onClicked: aboutButtonClicked()
        }
    }

    background: Rectangle {
        implicitHeight: units.gu(6)
        color: colors.background

        Rectangle {
            anchors {
                left: parent.left
                right: parent.right
                top: parent.bottom
            }

            height: units.dp(1)
            color: colors.divider
        }
    }
}
