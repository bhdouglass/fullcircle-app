function urlToId(url) {
    return url.replace('http://', '').replace('https://', '').replace(/\./g, '_').replace(/\//g, '_');
}
