import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Lomiri.Components 1.3 as Lomiri
import U1db 1.0 as U1db
import io.thp.pyotherside 1.4

import "utils.js" as Utils
import "Components"

Page {
    id: listPage
    title: i18n.tr('Full Circle Magazine')

    property var issues: []
    property bool err: false
    property bool loading: false

    Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl("../src"));

            importModule("main", function() {
                refresh();
            });
        }

        onError: {
            listPage.err = true;
            listPage.loading = false;
            console.log("python error: " + traceback);
        }
    }

    // TODO remove dependency on u1db
    U1db.Database {
        id: issuesdb
        path: 'fullcircle.bhdouglass.issues.v2'
    }

    U1db.Database {
        id: downloadsDb
        path: "fullcircle.bhdouglass.downloads"
    }

    function loadDownloads(issues) {
        var downloadMap = {};
        var docs = downloadsDb.listDocs();
        for (var index in docs) {
            var id = docs[index];
            downloadMap[id] = downloadsDb.getDoc(id);
        }

        for (var index in issues) {
            var issue = issues[index];
            issue.downloaded = false;

            for (var jindex in issue.downloads) {
                var download = issue.downloads[jindex];
                download.downloaded = false;

                // Handle legacy stuff
                if (download.link && !download.url) {
                    download.url = download.link;
                }

                download.id = Utils.urlToId(download.url);

                if (downloadMap[download.id]) {
                    issue.downloaded = true;
                    download.downloaded = true;
                    download.path = downloadMap[download.id].path;
                }
            }
        }

        return issues;
    }

    function loadIssues() {
        var issues = [];
        var docs = issuesdb.listDocs();
        for (var index in docs) {
            var issue = issuesdb.getDoc(docs[index]);

            if (issue && issue.title) {
                issues.push(issue);
            }
        }

        issues = loadDownloads(issues);

        var downloadedIssues = [];
        for (var jindex in issues) {
            if (issues[jindex].downloaded) {
                downloadedIssues.push(issues[jindex]);
            }
        }

        downloadedIssues.reverse();
        listPage.issues = downloadedIssues;
    }

    function refresh() {
        loadIssues();

        listPage.err = false;
        listPage.loading = true;

        python.call("main.fetchIssues", [], function(issues) {
            var ids = [];
            for (var index in issues) {
                issuesdb.putDoc(issues[index], issues[index].id);
                ids.push(issues[index].id);
            }

            // Remove unrecognized issues
            var docs = issuesdb.listDocs();
            for (var index in docs) {
                if (ids.indexOf(docs[index]) == -1) {
                    issuesdb.deleteDoc(docs[index]);
                }
            }

            issues = loadDownloads(issues);
            listPage.issues = issues;

            listPage.loading = false;
        });
    }

    // TODO cache images

    Flickable {
        anchors.fill: parent
        contentHeight: grid.implicitHeight + units.gu(4)
        clip: true

        ColumnLayout {
            id: indicator

            visible: listPage.loading || listPage.err
            height: listPage.err ? units.gu(10) : units.gu(4)

            anchors {
                top: parent.top
                right: parent.right
                left: parent.left
                topMargin: units.gu(2)
            }

            BusyIndicator {
                Layout.alignment: Qt.AlignHCenter

                visible: listPage.loading
                running: listPage.loading
            }

            Label {
                Layout.fillWidth: true

                visible: listPage.err
                text: i18n.tr('Something went wrong downloading the issue list')
                horizontalAlignment: Label.AlignHCenter
            }

            Button {
                Layout.alignment: Qt.AlignHCenter

                visible: listPage.err
                text: i18n.tr('Refresh')

                onClicked: refresh()
            }
        }

        GridLayout {
            id: grid
            anchors {
                top: indicator.visible ? indicator.bottom : parent.top
                right: parent.right
                left: parent.left
                topMargin: units.gu(2)
            }

            property double columnWidth: (width / columns)

            columnSpacing: 0
            rowSpacing: units.gu(4)
            columns: {
                if (listPage.width > listPage.height) {
                    return 4;
                }

                return 2;
            }

            Repeater {
                model: listPage.issues

                delegate: IssueTile {
                    Layout.preferredWidth: grid.columnWidth
                    Layout.preferredHeight: preferredHeight

                    issue: modelData

                    onClicked: stackView.push(Qt.resolvedUrl('IssuePage.qml'), {issue: modelData})
                }
            }
        }

        ScrollIndicator.vertical: ScrollIndicator { }
    }

    Connections {
        target: stackView
        onDepthChanged: {
            loadDownloads(listPage.issues);
            listPage.issuesChanged();
        }
    }
}
