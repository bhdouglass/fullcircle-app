import QtQuick 2.9
import QtQuick.Controls 2.2
import Lomiri.Components 1.3 as Lomiri
import Lomiri.Components.Popups 1.0
import Lomiri.Content 1.3

Page {
    id: openPage
    title: i18n.tr('Open With')

    property var activeTransfer
    property var path

    Rectangle {
        anchors.fill: parent

        ContentItem {
            id: exportItem
        }

        ContentPeerPicker {
            id: peerPicker
            visible: openPage.visible
            handler: ContentHandler.Destination
            contentType: ContentType.Documents
            showTitle: false

            onPeerSelected: {
                activeTransfer = peer.request();
                var items = [];
                exportItem.url = path;
                console.log(path);
                items.push(exportItem);
                activeTransfer.items = items;
                activeTransfer.state = ContentTransfer.Charged;

                stackView.pop();
            }

            onCancelPressed: {
                stackView.pop();
            }
        }
    }
}
